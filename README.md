# engineering-internship

Repository to store the activities carried out in the internship plan.

## Módulo 1 :white_check_mark:
* **Git e GitFlow**
* **Markdown**
* **SAAS, Css, Grid e Flexbox**
* **[Exercício - CSS Grid](https://gitlab.com/lucas.viana1/engineering-internship/-/blob/main/modulo-1/1-CSS_Grid/exerc%C3%ADcio-grid.css)**
* **[Exercício - FlexBox](https://gitlab.com/lucas.viana1/engineering-internship/-/blob/main/modulo-1/2-CSS_FlexBox/exerc%C3%ADcio-flexbox.css)**
* **[Exercício - Javascript](https://gitlab.com/lucas.viana1/engineering-internship/-/tree/main/modulo-1/3-Calculadora_Javascript)**

## Módulo 2 :white_check_mark:
* **[Exercício - React](https://gitlab.com/lucas.viana1/engineering-internship/-/tree/main/modulo-2/1-desafio_react)**
* **[SCRUM](https://gitlab.com/lucas.viana1/engineering-internship/-/tree/main/modulo-2/2-sobre_SCRUM)**
* **NPM e Yarn**
* **VTEX Overview**
* **API's e Postman**
* **[API's VTEX](https://gitlab.com/lucas.viana1/engineering-internship/-/tree/main/modulo-2/3-api's_postman)**

## Módulo 3 :white_check_mark:
* **Figma**
* **[Onboarding VTEX](https://gitlab.com/lucas.viana1/engineering-internship/-/tree/main/modulo-3/1-onboarding_vtex)**
* **VTEX IO: Conceitos**

## Módulo 4 :white_check_mark:
* **VTEX IO: Store Framework**
* **Store Framework Training**

## Módulo 5 :white_check_mark:
* **Email Framework**
* **Email Framework Training**

## Módulo 6 :white_check_mark:
* **Master Data v1**
* **Master Data v1 - Criando Entidades**

## Módulo 7 :white_check_mark:
* **Componentes React**
* **[Desenvolvendo Componentes](https://gitlab.com/acct.global/acct.vtexio-projects/react-app-training/-/tree/viana-app)**

## Módulo 8 :white_check_mark:
* **React Hooks**
* **Contextos React**
* **[Aplicando React Hooks](https://gitlab.com/acct.global/acct.vtexio-projects/react-app-training/-/tree/viana-app-context)**

## Módulo 9 :white_check_mark:
* **GraphQL**
* **Fazendo Queries e Mutations**

## Módulo 10 :white_check_mark:
* **Typescript**
* **[Aplicando Typescript](https://gitlab.com/acct.global/acct.vtexio-projects/react-app-training/-/tree/release/viana-app-context)**

## Módulo 11 :computer:
* **Testes Unitários**
* Aplicando Testes Unitários em VTEX IO

## Avaliação :hourglass:
* Entrega do Projeto Final
