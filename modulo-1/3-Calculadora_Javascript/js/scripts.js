//função que obtem os valores dos botões da calculadora
function getVal(num){
    var result = document.getElementById('result').value;
    //Esse if testa se alguma operação anterior foi alguma tentativa
    //de divisão por 0, caso sim, limpará o visor antes da digitação
    if(result=="Não é possível dividir por 0"){
       clean(); 
    }else{

    }
    //Visor da calculadora recebendo o que for digitado dos botões
    document.getElementById('result').value += num;
}

//Função que limpa o visor da calculadora
function clean(){
    document.getElementById('result').value = "";
}

//Função que limpa do visor o último valor digitado
function backspace(){
   var result = document.getElementById('result').value;
   document.getElementById('result').value = result.substring(0, result.length -1)
}

function calc(){
    var result = document.getElementById('result').value;
    //Testar se há valores digitados
    if(result){
        //Testar tentativas de divisão por 0 em que o resultado seria "Infinity"
        if(eval(result) === Infinity){
            document.getElementById('result').value = "Não é possível dividir por 0";
        }else{
        //A função eval() computa uma expressão passada em caracteres    
        document.getElementById('result').value = eval(result);
         }
    } else{

    }
}
