import Text from "./components/Text";

function App() {
  const formats = ["Normal","LowerCase","UpperCase","Split","Inverse","Doubleinverse","Point","Bar","BarInverse"];
  return (
    <>
    <h1 className="title">Desafio - React</h1>
    { formats.map( (format) => <Text text="Hello World" format={format}/>) } 
  </>
  )
}

export default App;
