const Text =({text, format}) =>{
    let data = new Date();
    let date = String(data);
    

    switch (format) {
        case 'Normal':
            return <>
            <h2>{text}</h2> 
            <h3>Data: {date}</h3>
            </>

        case 'LowerCase':
            return <>
            <h2>{text.toLowerCase()}</h2> 
            <h3>Data: {date}</h3>
            </>

        case 'UpperCase':
            return <>
            <h2>{text.toUpperCase()}</h2> 
            <h3>Data: {date}</h3>
            </>
            

        case 'Split':
            return <>
            <h2>{text.split(' ')}</h2> 
            <h3>Data: {date}</h3>
            </>

        case 'Inverse':
            return <>
            <h2>{text.split('').reverse().join('')}</h2> 
            <h3>Data: {date}</h3>
            </>

        case 'Doubleinverse':
            return <>
            <h2>{text.split("").reverse().join('').split(" ").reverse().join(" ")}</h2> 
            <h3>Data: {date}</h3>
            </>

        case 'Point':
            return <>
             <h2>{text.split('').join(".")}</h2> 
            <h3>Data: {date}</h3>
            </>


        case 'InversePoint':
            return <>
            <h2>{text.split('').reverse().join('.')}</h2> 
            <h3>Data: {date}</h3>
            </>

        case 'Bar':
            return <>
            <h2>{text.split('').join(' - ')}</h2> 
            <h3>Data: {date}</h3>
            </>

        case 'BarInverse':
            return <>
            <h2>{text.split('').reverse().join(' - ')}</h2> 
            <h3>Data: {date}</h3>
            </>

        default:
    }return
 
}
export default Text
 //split com ponto entre as letras