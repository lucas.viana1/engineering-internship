# O SCRUM
O Scrum é um framework baseado em metodologias trazidas do Manifesto Ágil. Criado na década de 90 por Jeff Sutherland e Ken Schwaber, foi desenvolvido com o objetivo de atender demandas de empresas de software, porém, hoje em dia, tem se difundido em empresas de diversos segmentos. Seus princípios e lições podem ser aplicados a todos os tipos de trabalhos em equipe. Esse é um dos motivos do Scrum ser tão popular.

É uma estrutura que ajuda as equipes a trabalharem juntas. Semelhante a uma equipe de rugby (de onde vem o nome) treinando para o grande jogo, o Scrum estimula as equipes a aprenderem com as experiências, a se organizarem enquanto resolvem um problema e a refletirem sobre os êxitos e fracassos para melhorarem sempre.
Segundo o Scrum Guide, o guia oficial da metodologia, **“Scrum é um framework dentro do qual pessoas podem tratar e resolver problemas complexos e adaptativos, enquanto produtiva e criativamente entregam produtos com o mais alto valor possível”**. Com isso tudo dito, fica claro que seu grande diferencial é prezar por uma entrega fracionada de um projeto, não visando apenas dinheiro e processos, mas também **a equipe**.

## A equipe no SCRUM
E falando de equipe, é essencial saber como um *Squad* se divide para aplicar o esse framework, conhecendo seus papéis e responsabilidades de cada um.

Numa equipe que utiliza Scrum, temos três pilares bem definidos: **Product Owner (PO)**, o **Time de Desenvolvimento** e o **Scrum Master**:

* **Product Owner (PO):** É conhecido como o "dono do produto", sendo responsável por garantir que as funcionalidades do produto em desenvolvimento sejam entregues na ordem que reflitam o maior valor possível para o cliente. Para que isso possa ocorrer, é essencial que o Product Owner gerencie corretamente o **Product Backlog**, que é uma lista de demandas com os desejos do cliente.

* **Time de Desenvolvimento:** São os integrantes da equipe responsáveis pela concepção e sustentação do produto, ou seja, desenvolvedores, analistas de qualidade, designers, etc. O time deve ser auto organizável e com poder de decisão suficiente para viabilizar tecnicamente um incremento liberável para uso da funcionalidade em desenvolvimento.

* **Scrum Master:** Também conhecido como Agilista, é responsável por guiar todo o time no uso do framework Scrum, no entendimento das suas teorias, ritos e valores. A liderança do time por meio do Scrum Master acontece de uma maneira diferente da gestão tradicional, pois o SM deve agir como um líder servidor, ou seja, fazendo com que todos daquele time tenham o entendimento das boas práticas e também blindando o time de interferências externas desnecessárias. isso reforça mais ainda a ideia de pensar **na equipe**, pois é o SM que deve servir aos integrantes e não o contrário.

## Cerimônias do SCRUM
As cerimônias do Scrum são essenciais para que o framework ocorra em sua plenitude, favorecendo a constância do time em relação às entregas e também levando-o para um estado de evolução recorrente por meio das inspeções e reflexões ao final de cada ciclo.

**As cerimônias do SCRUM são as seguintes:**
* Sprint (Evento)
* Sprint Planning
* Daily  Scrum
* Sprint Review
* Sprint Retro


![cerimônias do SCRUM](https://f.hubspotusercontent30.net/hubfs/8797985/Imported_Blog_Media/sprint.png)

### Sprint
A Sprint, pode ser considerada, mais um evento do framework, do que uma cerimônia por si só, pois é através dela, que as demais se desenrolam durante o projeto, por isso, ela engloba as outras quatro cerimônias existentes. Se trata de um período em que o time do Scrum se dedica para entregar algo de valor para o cliente, como por exemplo, uma nova funcionalidade de um site. Esse período deve durar de uma a quatro semanas, por definição a ser acordada entre os Squads.
* **O Kanban na Sprint:** Kanban é um termo da língua japonesa que significa “cartão”. Foi criado na década de 60 com o propósito de controlar o estoque de uma fábrica, para que não faltasse e nem sobrassem materiais em seu estoque. Seu esquema organizacional é feito por meio de cartões e colunas. As colunas representam os materiais/suprimentos, por exemplo, e a cor dos cartões o seu nível de necessidade, num esquema de mover os cartões da esquerda para a direita, auxiliando no controle das tarefas num projeto que utiliza SCRUM.
![quadro KANBAN](https://res.cloudinary.com/monday-blogs/w_908,h_602,c_fit/fl_lossy,f_auto,q_auto/wp-blog/2020/10/image8-4.png)

### Sprint Planning
O Sprint Planning é o momento em que a equipe se reúne para discutir o que será realizado na Sprint. O time define em conjunto, a partir das prioridades apontadas pelo P.O. e análise técnica do time, qual será o **objetivo** desta interação. Esse objetivo irá guiar as decisões tomadas pelo time durante toda a Sprint, ajudando a manter o time **focado na entrega de valor** e flexível para se adaptar caso imprevistos aconteçam. 

Em seguida, o P.O. apresenta as **histórias de usuário** priorizadas para este ciclo e discute com o time a viabilidade de sua implementação. Logo após, os desenvolvedores quebram as histórias em **tarefas técnicas** e realizam as **estimativas**, para ter uma previsão do que parece possível de ser desenvolvido nesta Sprint e vale lembrar que PO e SM não votam nessa cerimônia de estimativas.

### Daily Scrum
As dailies são o momento em que o time se reune brevemente, todos os dias, para bolar um plano sobre o que será feito no dia de hoje para atingir o objetivo da Sprint. É o momento de inspecionar o andamento das atividades e ajustar a rota caso necessário. É uma reunião super objetiva, mantendo um time box de 15 minutos. Para que todos se coloquem na mesma página, cada um fala brevemente sobre o que realizou desde a última daily e o que pretende realizar hoje, sinalizando se está com algum problema. Os membros trocam ideia sobre quais são os próximos passos necessários para resolver os problemas apontados. Caso seja necessário aprofundar alguma discussão, os membros necessários combinam de fazê-la em outro momento, para não estourar o tempo e travar toda a equipe. Também é importante ser dito o que foi feito no dia anterior, e se há algum impedimento nas tarefas que comprometa a entrega de qualidade do projeto.

### Sprint Review
A revisão da Sprint é um momento muito importante de colaboração. Após uma Sprint de desenvolvimento, o time se reúne com stakeholders disponibilizando o produto para inspeção. Não é uma reunião de status, então o foco está em receber o feedback sobre o incremento e discutir em conjunto os próximos passos. O time utiliza o feedback para refletir no que pode ser melhorado, traçando um plano de ação.

### Retrospectiva da Sprint

Para **melhorar continuamente**, o time se reúne toda Sprint e analisa a Sprint que se encerra. Olhando para diferentes aspectos - indivíduos, interações, processo, qualidade etc - a equipe reflete no que foi bom e no que pode ser melhorado. Aqui, o objetivo não é apontar culpados, mas sim analisar os **problemas** de forma objetiva e pensar em **soluções** que possam ser postas em prática imediatamente. Dessa forma, o time bola um **plano de ação** para melhorar já na próxima Sprint.

## Artefatos do Scrum
No Scrum, um artefato é algo que se cria, por exemplo uma ferramenta para solucionar um problema. Há três artefatos no Scrum: o backlog do produto, o backlog da sprint e o incremento do produto.
![Artefatos](https://blog.scrumstudy.com/wp-content/uploads/Product-Owner_111.jpg)

* **O Backlog do Produto:** O backlog do produto consiste na lista mestra do trabalho que precisa ser feito, e o gestor de projeto ou o proprietário do produto deve fazer a triagem dela. Os itens listados, porém, não são necessariamente aqueles em que a sua equipe trabalhará, ou melhor, os itens no backlog do produto são opções em que a sua equipe pode trabalhar durante o sprint do Scrum. Os proprietários do projeto devem reordenar e atualizar com frequência o backlog do produto, com base nas novas informações obtidas dos clientes, do mercado ou da equipe do projeto.

* **O Backlog da Sprint:** O backlog do sprint é a coleção de trabalhos ou produtos com a qual a sua equipe se comprometeu pelo tempo de duração do sprint do Scrum. Esses itens são escolhidos dentre as atividades do backlog do produto, durante a sessão de planejamento do sprint, e transferidos para o projeto de planejamento de sprints, se vocês tiverem um.

Talvez o squad não consiga entregar todo os itens do backlog planejados para cada sprint, mas é improvável que vocês aumentem o backlog do sprint a meio caminho. Caso essa situação ocorra com frequência, dedique mais tempo à fase de planejamento do sprint para ter uma ideia concreta da abrangência dos trabalhos durante o sprint.

* **Incremento do produto:** O incremento do produto é aquilo que se entrega ao fim de um sprint, o que pode ser um novo produto ou recurso, uma melhoria, a correção de um bug ou qualquer outra coisa que dependa da sua equipe. Planeje a apresentação do incremento durante a revisão do sprint. A essa altura, ele será entregue ou não com base na opinião dos participantes sobre o incremento, e se ele pode ser considerado como “Feito”.

## A importância do Tempo
Como tudo tem um propósito e um objetivo muito claro com Scrum, é interessante que o tempo seja bem organizado. Todo minuto importa!

Tenha em mente, porém, que o Scrum sempre preza pela flexibilidade no desenvolvimento dos projetos. Se sentir que algo não está funcionando da maneira que deveria, talvez seja importante dar um passo para trás e refletir!

Como está escrito no manifesto: “*Mudanças nos requisitos são bem-vindas, mesmo tardiamente no desenvolvimento. Processos ágeis tiram vantagem das mudanças visando vantagem competitiva para o cliente”.

# Concluindo...
O Scrum não é para todos, mas tampouco deve se limitar apenas às equipes de produto, desenvolvimento de software e engenharia. Qualquer equipe pode adotar a estrutura do Scrum e usar a noção de melhoria contínua para realizar um ótimo trabalho. Veja algumas vantagens e desvantagens do uso do Scrum:

## Vantagens do Scrum
O Scrum é mais eficaz para equipes que precisam frequentemente criar ou entregar itens, sejam eles “produtos” tradicionais, como código ou novas funcionalidades, sejam “produtos” Scrum mais incomuns, como campanhas de marketing ou peças e ativos criativos.

As equipes que seguem a estrutura do Scrum tiram proveito da sua agilidade e flexibilidade. O processo do Scrum pode ajudar a aumentar a colaboração em equipe e atingir as suas metas de forma mais eficaz. Além disso, as equipes do Scrum se mantêm sempre cientes do próprio trabalho, uma vez que elas extraem as tarefas do backlog do produto e têm clareza dos próprios objetivos, já que todos estão de acordo sobre o que significa um trabalho “Feito”.

## Limitações do Scrum
Com frequência, os projetos do Scrum sofrem de desvios de escopo, porque a própria natureza deles envolve e encoraja a mudança. Caso haja alterações de mais, ou caso recebam mais comentários desaprovadores dos clientes do que o normal, talvez a sua equipe esteja fazendo repetidas iterações sem obter resultados reais.

* Solução: defina com clareza os objetivos e o incremento de cada sprint. Além disso, assegure que toda a sua equipe do Scrum tenha o mesmo entendimento da noção de “Feito”, para que ela não continue trabalhando depois de atingir esse ponto. Se for necessário, implemente um processo de controle de mudança para evitar esses problemas.

As equipes do Scrum fazem muitas reuniões. Além dos agendamentos recorrentes de planejamento e revisão de sprint, as equipes de Scrum também fazem encontros diários.

* Solução: caso as suas reuniões diárias de Scrum não pareçam úteis, encontre um jeito de torná-las mais produtivas. Fazer o acompanhamento dos resultados das reuniões em um projeto pode ajudar a que se concentrem apenas nos aspectos mais úteis.

O Scrum pode ser difícil (embora não impossível) de implementar se você não fizer parte de uma equipe de produto, engenharia ou desenvolvimento de software.

* Solução: caso a sua equipe decida usar o Scrum, esclareça exatamente como os processos do Scrum poderão ajudar. Se possível, identifique os pontos a melhorar e indique momentos e encontros do Scrum que possam ser úteis. Planeje também várias sessões de treinamento durante os primeiros sprints do Scrum a fim de ajudar a sua equipe a obter êxito.