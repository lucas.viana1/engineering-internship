# Onboarding VTEX

Módulo para estudo dos conceitos de ecommerce, importância, segmentação e estrutura da VTEX nesse ramo de mercado.

## Introdução

* Apenas 6% das vendas totais do varejo, são feitas de forma online aqui no Brasil.
* Conceito de "Phygital".
* Compras online com retirada em loja.
* Magazine Luiza, Mercado Livre e Casas Bahia no ecommerce brasileiro.
* Custo de criar uma plataforma ecommerce do 0 é muito alto e demanda muito tempo.
* VTEX é a maior plataforma de ecommerce da América Latina, em constante expansão global.
* Surgiu no ano de 2017. Atendendo a um perfil de clientes maiores.

## Catálogo

* Primeiro nível de categorias é chamado de "departamento". Dentro há categorias e subcategorias, o ideal é que não passe de 3 níveis.
* Incluir similar, mostra produtos em outra categoria selecionada.
* Grupo são sessões dentro da PDP, como especificações de Produto.
* Campos são características do produto.
* SKU são variações do produto.

* Nome do produto vai na URL, por isso não é usada acentuação.
* Palavras substitutas são termos que também buscarão pela categoria.
* tag title e meta tag são relacionadas a SEO.
* Adwords e Lomadee são relacionados a campanhas.

* A marca é obrigatória para cadastro de qualquer produto.
* Titulo e descrição da marca alimentam os motores de busca.
* É possivel importar e exportar numa planilha, os dados em dos produtos.

* Anexos são customizações nos produtos.
* Anexos ficam atrelados aos pedidos.
* Alguns anexos como os de recorrência, precisam de uma chave específica.
* O tipo de serviço define onde o anexo será exibido.

## Preços

* Lista de preços, precifica os produtos por SKU.
* É possível precificar de acordo com a política comercial estabelecida.

* O preço base é geral feito pelo cálculo de markup.
* Markup é o lucro sobre o preço de custo.
* preço de lista = "de 300 por 200".

* É possivel criar regras de preço, dando descontos ou acrescimos no preço, de acordo com o produto, categoria ou meio de pagamento.
* Pode ser estabelecido um tempo específico para a aplicação dessa regra.
* Preço psicológico : 100 = 99,90 ou 99,99.

## Promoções e Taxas

* Campanhas são containers de promoções.
* Definem regras que ativam as promoções.
* É possível criar públicos alvo.

* Cupons são códigos, que aplicados no carrinho, ativam uma promoção.
* Recebem um código e utm.
* É preciso Setar o cupom na promoção.

* Taxas são o contrário de promoções.
* Utilizadas para cobrar impostos ou valores que sejam necessários de acordo com a necessidade.
* Pode ser percentual, nominal, fórmula ou relacionada ao frete.

## Configurações da Conta

* Gerenciamento de conta, configura acessos e permissões ao ambiente.
* Perfil Owner pode fazer quase tudo.

* Primeiro contato do logista à plataforma.
* Após criação dos perfis de acesso, são criados os usuários.
* E então é criada a conta da loja ou ambiente.
* Podem ser criadas lojas.

* Apenas o usuário mestre pode criar chaves de segurança.
* Autenticação trabalha com códigos de acesso.
* Pro admin há opção de Facebook e Google.

## Pedidos

* Gerenciamento de pedidos da loja.
* É possível aplicar diversos filtros.

* Pode-se ver a quantidade de items por pedido.
* Os dados do cliente são apresentados no pedido.
* Há todo um fluxo de pedido.
![Fluxo de Pedido](https://gitlab.com/lucas.viana1/engineering-internship/-/raw/main/modulo-3/1-onboarding_vtex/Screenshot_2.png)
* É possível ver os dados de pagamento.
* É possível setar uma quantidade mínima de algum produto para adicionar ao carrinho

* Uma doca é o ponto de origem, de onde será retirado o produto para envio.
* Uma doca pode receber produto de vários estoques.
* É necessário informar o endereço da doca.
* O calculo de frete tem origem com o endereço da doca.

* Há como configurar os locais de estoque da loja.
* É possível exportar um Csv do estoque de produtos.
* É necessário atrelar os estoques às docas.

## Pagamentos

* Aqui é feito o gerenciamento de transações de cada pedido.
* É possível filtrar o status de cada transação.

* Vales compra são códigos gerados e usados no checkout como forma de pagamento.
* São usados como gift card por exemplo.

* Há meios de pagamento e afiliações de gateway.
* Gateway faz conexão entre o adquirente e a VTEX para meios de pagamento.
* Há vários tipos de conectores.

## MarketPlace 

* Sellers e Integrações.
* Seller é a loja proprietária do produto.
* Venda e cobrança são feitas pelo marketplace. 

## Configurações da loja

* Políticas comerciais definem regras de valor a cada país. 
* A partir do VTEX Io as alterações não são feitas diretamente no HTML.

* React e componentização.
* Costumização de componentes.
* Configuração de PWA.
* **Site Editor.**

## Cliente

* Central de mensagens armazena templates de emails institucionais.
* Remetente diz respeito a quem enviará o email.

* Master Data é um banco de dados próprio da VTEX.
* Aplicações são conjuntos de entidades.
